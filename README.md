# 123L Website

## Requirements
- [Elm][elm-install]
- Node
- Make

## Usage

```bash
elm reactor
```

 [elm-install]: https://guide.elm-lang.org/install/elm.html
