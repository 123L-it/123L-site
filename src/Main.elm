module Main exposing (..)

import Browser
import Browser.Navigation as Nav
import Html exposing (..)
import Html.Attributes exposing (..)
import Url



-- MAIN


main : Program () NavItem Msg
main =
    Browser.application
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        }



-- MODEL


type alias NavItem =
    { key : Nav.Key
    , url : Url.Url
    }


init : () -> Url.Url -> Nav.Key -> ( NavItem, Cmd Msg )
init _ url key = (NavItem key url, Cmd.none)



-- UPDATE


type Msg
    = LinkClicked Browser.UrlRequest
    | UrlChanged Url.Url


update : Msg -> NavItem -> (NavItem, Cmd Msg)
update msg navItem =
    case msg of
        LinkClicked urlRequest ->
            case urlRequest of
                Browser.Internal url ->
                    (navItem, Nav.pushUrl navItem.key (Url.toString url))

                Browser.External href ->
                    (navItem, Nav.load href)

        UrlChanged url ->
            ({ navItem | url = url }
            , Cmd.none)


-- SUBSCRIPTIONS


subscriptions : NavItem -> Sub Msg
subscriptions _ =
    Sub.none

-- VIEW


view : NavItem -> Browser.Document Msg
view navItem =
    { title = "Url Title"
    , body =
          [ text "The current URL is: "
                , b [] [text (Url.toString navItem.url)]
                , nav []
                [ ul []
                      [ viewLink "/home"
                      , viewLink "/about"
                      , viewLink "/contact"
                      ]
                ]
          ]
    }

viewLink : String -> Html msg
viewLink path =
    li [] [a [href path] [text path]]
