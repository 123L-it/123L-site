.PHONY: setup

setup:
	npm install --no-package-lock --no-save \
        @elm-tooling/elm-language-server@2.7.0 \
		elm-test@0.19.1-revision12 \
		elm-review@2.10.2 \
		elm-format@0.8.7
